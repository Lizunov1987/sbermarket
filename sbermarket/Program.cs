﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.OffScreen;
using Newtonsoft.Json;
using sbermarket;

namespace test
{
    class Program
    {
        public  static async Task Main(string[] args)
        {
            // Иницилизация ядра chromium
            Cef.Initialize(new CefSettings()
            {
                CachePath = Path.GetFullPath("cache"),
                LogSeverity = LogSeverity.Disable
            });
            var sbermarketParser = new SbermarketParser(new ProductParsedValidator());
            var facade = new ZelenoeyablokoFacade();
            
            var sections = await facade.SectionResources.ListAsync();

            var sectionNumber = GetStartSectionNumber(sections.ToArray());
            
            var categories = await facade.CategoryResources.ListAsync(sections[sectionNumber].Id);
            var categoryNumber = GetStartCategoryNumber(categories.ToArray());
            
            for (var i = sectionNumber; i < sections.Count; i++)
            {
                if(i.OneOf(19, 24, 28, 29, 30))
                    continue;

                var section = sections[i];
                categories = await facade.CategoryResources.ListAsync(section.Id);
                
                for(var k = categoryNumber; k < categories.Count; k++)
                {
                    var category = categories[k];
                    
                    var pathToFolder = Path.Combine(Directory.GetCurrentDirectory(), category.Name_Ru);
                    CreateFolderIfNeeded(pathToFolder);

                    using (var logger = new Logger(Path.Combine(pathToFolder, $"log_{category.Id}.txt")))
                    {
                        var products = await facade.ProductResources.ListAsync(category.Id);
                        for(var j = 0; j < products.Count; j++)
                        {
                            
                            var product = products[j];
                            if(product.ImageUrls != null && product.ImageUrls.Any())
                            {
                                logger.Write($"ТОВАР УЖЕ ПРИСУТСТВУЕТ В БАЗЕ: {j + 1}/{products.Count}: {product.Name}");
                                continue;
                            }

                            logger.Write("");
                            logger.Write($"НАЧИНАЕМ ПАРСИТЬ ТОВАР {j + 1}/{products.Count}: {product.Name}");
    
                            try
                            {
                                var parsedResult = await sbermarketParser.ParseProductByNameAsync(product.Name);
                                var productParsed = parsedResult.ProductParsed;
                                var validationErrors = parsedResult.ValidationErrors;
                                
                                var pathToFile = Path.Combine(pathToFolder, $"{product.Id}.json");
                                using (var file = File.CreateText(pathToFile))
                                {
                                    var serializer = new JsonSerializer();
                                    serializer.Serialize(file, productParsed);
                                }
                                
                                logger.Write("", validationErrors.Any());
                                logger.Write($"ТОВАР РАСПАРСИЛСЯ {j + 1}/{products.Count}: {product.Name}", validationErrors.Any());
                                logger.Write("ССЫЛКА НА ТОВАР(СБЕРМАРКЕТ):", validationErrors.Any());
                                logger.Write(productParsed.Url, validationErrors.Any());
                                logger.Write("НАЗВАНИЕ ТОВАРА(СБЕРМАРКЕТ):", validationErrors.Any());
                                logger.Write(productParsed.Name, validationErrors.Any());
    
                                if (validationErrors.Any())
                                {
                                    logger.Write("ПРЕДУПРЕЖДЕНИЯ:", validationErrors.Any());
                                    logger.Write(validationErrors);
                                }

                                var updateProduct = new UpdateProductModel(category.Id, product.Id, product.Name, productParsed);
                                await facade.ProductResources.UpdateAsync(updateProduct);
                            }
                            catch (Exception e)
                            {
                                logger.Write("", true);
                                logger.Write($"ТОВАР НЕ УДАЛОСЬ РАСПАРСИТЬ {j + 1}/{products.Count}: {product.Name}", true);
                                continue;
                            }
                        }
                    }

                }

                categoryNumber = 0;
            }
        }
        
        private static void CreateFolderIfNeeded(string pathToFolder)
        {
            if (!Directory.Exists(pathToFolder))
                Directory.CreateDirectory(pathToFolder);
        }

        private static int GetStartSectionNumber(SectionModel[] sections)
        {
            while(true)
            {
                Console.WriteLine("|-------|-----------------------------------------------------|");
                Console.WriteLine("| Номер |                 Наименование секции                 |");
                Console.WriteLine("|-------|-----------------------------------------------------|");

                for (int i = 0; i < sections.Length; i++)
                    Console.WriteLine("| {0,5} | {1} {2}|", i, sections[i].Name, new string(' ', 51 - sections[i].Name.Length));

                Console.WriteLine("|-------|-----------------------------------------------------|");

                Console.WriteLine("Введите номер секции");
                if (int.TryParse(Console.ReadLine(), out var sectionNumber) && (sectionNumber < 0 || sectionNumber > sections.Length))
                {
                    Console.WriteLine("Неверный номер секции");
                    continue;
                }
                if (sectionNumber.OneOf(19, 24, 28, 29, 30))
                {
                    Console.WriteLine("Данные секции нельзя выбрать");
                    continue;
                }

                return sectionNumber;
            }
        }

        private static int GetStartCategoryNumber(CategoryModel[] categories)
        {
            while(true)
            {
                Console.Clear();
                Console.WriteLine("|-------|-----------------------------------------------------|");
                Console.WriteLine("| Номер |                 Наименование категории              |");
                Console.WriteLine("|-------|-----------------------------------------------------|");

                for (int k = 0; k < categories.Length; k++)
                    Console.WriteLine("| {0,5} | {1} {2}|", k, categories[k].Name_Ru, new string(' ', 51 - categories[k].Name_Ru.Length));

                Console.WriteLine("|-------|-----------------------------------------------------|");

                Console.WriteLine("Введите номер категории");
                if (int.TryParse(Console.ReadLine(), out var categoryNumber) && (categoryNumber < 0 || categoryNumber > categories.Length))
                {
                    Console.WriteLine("Неверный номер категории");
                    continue;
                }
                
                return categoryNumber;
            }
        }
    }
}