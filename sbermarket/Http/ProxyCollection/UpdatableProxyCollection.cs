﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace test
{
	internal class UpdatableProxyCollection<T> : IProxyCollection<T> where T : IProxyService
	{

		private IList<Proxy> _currentProxyList = new List<Proxy>();
		
		private int _currentProxyIndex = 0;

		private object _lockObject = new object();
		
		public UpdatableProxyCollection(T proxyService)
		{
			// Запускаем таймер обновления списка прокси серверов
			var timer = new Timer(
				async(_) => UpdateProxyList(await proxyService.GetProxyListAsync()), 
				null, 
				0, 
				100000);

			// Подписываемся на изменение прокси листа
			proxyService.ProxyListChanged += UpdateProxyList;
		}
		
		private void UpdateProxyList(IList<Proxy> proxyList)
		{
			lock (_lockObject)
			{
				_currentProxyList = proxyList;
			}
		}

		public Proxy GetRandomProxyServer()
		{
			if(!_currentProxyList.Any())
				return Proxy.Localhost;

			lock(_lockObject)
			{
				_currentProxyIndex++;
				if (_currentProxyIndex >= _currentProxyList.Count)
					_currentProxyIndex = 0;
			
				return _currentProxyList[_currentProxyIndex];
			}
		}
	}
}
