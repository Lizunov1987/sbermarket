﻿
namespace test
{
	internal interface IProxyCollection<T> where T : IProxyService
	{
		Proxy GetRandomProxyServer();
	}
}
