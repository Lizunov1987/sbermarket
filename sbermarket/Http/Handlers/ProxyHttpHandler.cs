﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace test
{
	internal class ProxyHttpHandler<T> : HttpClientHandler where T : IProxyService
	{
		private readonly IProxyCollection<T> _proxyCollection;
		
		private readonly WebProxy _proxyResolver;
		

		public ProxyHttpHandler(IProxyCollection<T> proxyCollection)
		{
			_proxyCollection = proxyCollection;
			// Разрешаем первый прокси сервер
			var proxy = proxyCollection.GetRandomProxyServer();
			_proxyResolver = new WebProxy(proxy.Host, proxy.Port);
			
			Proxy = _proxyResolver;
			UseProxy = true;
		}

		protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			// Резолвим новый прокси сервер
			var proxy = _proxyCollection.GetRandomProxyServer();
			_proxyResolver.Address = new Uri("http://" + proxy.Host + ":" + proxy.Port.ToString(CultureInfo.InvariantCulture));
			_proxyResolver.Credentials = proxy.Credentials;

			return base.SendAsync(request, cancellationToken);
		}
	}
}
