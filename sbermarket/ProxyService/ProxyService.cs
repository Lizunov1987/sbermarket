﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace test
{
	public class ProxyService : IProxyService
	{
		public event Action<IList<Proxy>> ProxyListChanged = delegate { };

		public const string ApiUrl = "http://proxys.io/ru/api/v2/ip?key=";

		public const string Key = "17f295e8cebba0dabe5097ea2eef936f";
		
		public async Task<IList<Proxy>> GetProxyListAsync()
		{
			var url = ApiUrl + Key;
			using(var http = new HttpClient())
			{
				var jObject = await http.GetJsonAsync(url);

				var username = jObject["data"]["username"].Value<string>();
				var password = jObject["data"]["password"].Value<string>();
				var proxyArray = (JArray)jObject["data"]["list_ip"];

				return  proxyArray
					.Select(proxyToken => new Proxy()
					{
						Host = proxyToken["ip"].Value<string>(),
						Port = proxyToken["port_http"].Value<int>(),
						Credentials = new NetworkCredential(username, password)
					})
					.ToList();
			}
		}
	}
}
