﻿using System.Net;

namespace test
{
	public class Proxy
	{
		public string Host { get; set; }


		public int Port { get; set; }
		
		public ICredentials Credentials { get; set; }
		
		public static Proxy Localhost => new Proxy() { Host = "127.0.0.1", Port = 80 };
	}
}
