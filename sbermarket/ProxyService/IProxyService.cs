﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace test
{

	public interface IProxyService
	{
		event Action<IList<Proxy>> ProxyListChanged;

		Task<IList<Proxy>> GetProxyListAsync();
	}
}
