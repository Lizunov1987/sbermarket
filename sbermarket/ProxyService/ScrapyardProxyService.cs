﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace test
{
	public class ScrapyardProxyService : IProxyService
	{
		public const string ApiUrl = "https://www.proxy-list.download/api/v1/get?type=http";

		public event Action<IList<Proxy>> ProxyListChanged = delegate { };

		public async Task<IList<Proxy>> GetProxyListAsync()
		{
			using(var http = new HttpClient())
			{
				var response = await http.GetAsync(ApiUrl);

				response.EnsureSuccessStatusCode();

				var content = await response.Content.ReadAsStringAsync();
				var proxyStrings = content.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

				var proxyServers = new List<Proxy>();
				foreach(var proxyString in proxyStrings)
				{
					var proxyPart = proxyString.Split(new char[] { ':' } );
					proxyServers.Add(new Proxy()
					{
						Host = proxyPart[0],
						Port = int.Parse(proxyPart[1])
					});
				}

				return proxyServers;
			}
		}
	}
}
