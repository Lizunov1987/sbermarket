using System;
using System.Net.Http;

namespace test
{
    public class ZelenoeyablokoFacade
    {
        private const string ApiKey = "IoDaz6RfC6oGQQE9dRjtSvPS1fuJAiII";

        private readonly HttpClient _httpClient = new HttpClient() { BaseAddress = new Uri("http://zelenoeyabloko.peretz.loya24.com:58083/")};
        
        public ZelenoeyablokoSectionResources SectionResources { get; }
        
        public ZelenoeyablokoCategoryResources CategoryResources { get; }

        public ZelenoeyablokoProductResources ProductResources { get; }
        
        public ZelenoeyablokoFacade()
        {
            SectionResources = new ZelenoeyablokoSectionResources(_httpClient, ApiKey);
            CategoryResources = new ZelenoeyablokoCategoryResources(_httpClient, ApiKey);
            ProductResources = new ZelenoeyablokoProductResources(_httpClient, ApiKey);
        }
    }
}