using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace test
{
    public class ZelenoeyablokoProductResources
    {
        private readonly HttpClient _httpClient;
        
        private readonly string _apiKey;

        public ZelenoeyablokoProductResources(HttpClient httpClient, string apiKey)
        {
            _httpClient = httpClient;
            _apiKey = apiKey;
        }
        
        public async Task<ProductModel> GetAsync(string productId)
        {
            if (string.IsNullOrEmpty(productId)) throw new ArgumentNullException(nameof(productId));

            var url = $"rest/delivery/getProduct?_apikey={_apiKey}";
            var body = new { ProductId = productId };
            var jObject = await _httpClient.PostJsonAsync(url, body);

            if (jObject.FieldExists("response"))
                return jObject["response"].ToObject<ProductModel>();
            throw new Exception("The response field doesn't exists in the api response");
        }

        public async Task<IList<ProductModel>> ListAsync(string categoryId)
        {
            if (string.IsNullOrEmpty(categoryId)) throw new ArgumentNullException(nameof(categoryId));
            
            var url = $"rest/delivery/getProducts?_apikey={_apiKey}";
            var body = new { CategoryId = categoryId };
            var jObject = await _httpClient.PostJsonAsync(url, body);

            if (jObject.FieldExists("response"))
                return jObject["response"].ToObject<IList<ProductModel>>();

            throw new Exception("The response field doesn't exists in the api response");
        }

        public async Task UpdateAsync(UpdateProductModel productUpdate)
        {
            var url = $"rest/delivery/saveProduct?_apikey={_apiKey}";
            var response = await _httpClient.PostJsonAsync(url, productUpdate);

            if (response.FieldExists("error"))
                throw new Exception("������ � ������� �� ���������� ������");
        }
    }
}