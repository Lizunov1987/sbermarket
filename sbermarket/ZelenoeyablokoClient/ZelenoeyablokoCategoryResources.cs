using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace test
{
    public class ZelenoeyablokoCategoryResources
    {
        private readonly HttpClient _httpClient;
        
        private readonly string _apiKey;

        public ZelenoeyablokoCategoryResources(HttpClient httpClient, string apiKey)
        {
            _httpClient = httpClient;
            _apiKey = apiKey;
        }
        
        public async Task<IList<CategoryModel>> ListAsync(string sectionId = null)
        {
            var url = $"rest/delivery/getCategories?_apikey={_apiKey}";
            var body = string.IsNullOrEmpty(sectionId) ? (object)new { } : (object)new { SectionId = sectionId };
            var jObject = await _httpClient.PostJsonAsync(url, body);

            if (jObject.FieldExists("response"))
                return jObject["response"].ToObject<IList<CategoryModel>>();

            throw new Exception("The response field doesn't exists in the api response");
        }
    }
}