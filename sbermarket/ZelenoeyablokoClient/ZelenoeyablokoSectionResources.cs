using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace test
{
    public class ZelenoeyablokoSectionResources
    {
        private readonly HttpClient _httpClient;
        
        private readonly string _apiKey;

        public ZelenoeyablokoSectionResources(HttpClient httpClient, string apiKey)
        {
            _httpClient = httpClient;
            _apiKey = apiKey;
        }
        
        public async Task<IList<SectionModel>> ListAsync()
        { 
            var url = $"rest/delivery/getSections?_apikey={_apiKey}";
            var jObject = await _httpClient.GetJsonAsync(url);

            if (jObject.FieldExists("response")) 
                return jObject["response"].ToObject<IList<SectionModel>>();

            throw new Exception("The response field doesn't exists in the api response");
        }
    }
}