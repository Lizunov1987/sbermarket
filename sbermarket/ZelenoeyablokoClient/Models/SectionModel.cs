namespace test
{
    public class SectionModel
    {
        public string Id { get; set; }
        
        public string Name { get; set; }
        
        public string ExternalId { get; set; }
    }
}