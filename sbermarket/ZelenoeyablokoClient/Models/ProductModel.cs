using Newtonsoft.Json;
using System.Collections.Generic;

namespace test
{
    public class ProductModel
    {
        public string Id { get; set; }

        [JsonProperty("name_ru")]
        public string Name { get; set; }
        
        [JsonProperty("description_ru")]
        public string Description { get; set; }

        [JsonProperty("composition_ru")]
        public string Ingredients { get; set; }

        [JsonProperty("energyValue_ru")]
        public string Caloric { get; set; }

        [JsonProperty("externalImgUrls")]
        public IList<string> ImageUrls { get; set; }

        [JsonProperty("compositionPerWeight_ru")]
        public string CompositionPerWeight { get; set; }

        [JsonProperty("weight_ru")]
        public string Weight { get; set; }

        [JsonProperty("lifetime_ru")]
        public string ShelfLife { get; set; }

        [JsonProperty("conditions_ru")]
        public string ConditionsStorage { get; set; }
    }
}