using Newtonsoft.Json;
using System.Collections.Generic;

namespace test
{
    public class UpdateProductModel
    {
        [JsonProperty("productId")]
        public string ProductId { get; set; }

        [JsonProperty("name_ru")]
        public string Name_ru { get; set; }

        [JsonProperty("categoryId")]
        public string CategoryId { get; set; }

        [JsonProperty("description_ru")]
        public string Description { get; set; }

        [JsonProperty("composition_ru")]
        public string Ingredients { get; set; }
        
        [JsonProperty("energyValue_ru")]
        public string Caloric { get; set; }

        [JsonProperty("externalImgUrls")]
        public IList<string> ImageUrls { get; set; }
       
        [JsonProperty("compositionPerWeight_ru")]
        public string CompositionPerWeight { get; set; }

        [JsonProperty("weight_ru")]
        public string Weight { get; set; }
        
        [JsonProperty("lifetime_ru")]
        public string ShelfLife { get; set; }
        
        [JsonProperty("conditions_ru")]
        public string ConditionsStorage { get; set; }

        public UpdateProductModel(string categoryId, string productId, string productName, ProductParsed productParsed)
        {
            Name_ru = productName;
            CategoryId = categoryId;
            ProductId = productId;
            Description = productParsed.Description;
            Ingredients = productParsed.Ingredients;
            Caloric = productParsed.Caloric;
            ImageUrls = productParsed.ImageUrls;
            Weight = productParsed.Weight;
            ShelfLife = productParsed.ShelfLife;
            ConditionsStorage = productParsed.ConditionsStorage;
            CompositionPerWeight = 
                (!string.IsNullOrEmpty(productParsed.Fat) ? $"����: {productParsed.Fat}, " : string.Empty) +
                (!string.IsNullOrEmpty(productParsed.Protein) ? $"�����: {productParsed.Protein}, " : string.Empty) +
                (!string.IsNullOrEmpty(productParsed.Carbohydrates) ? $"��������: {productParsed.Carbohydrates}" : string.Empty);
        }
    }
}