namespace test
{
    public class CategoryModel
    {
        public string Id { get; set; }
        
        public string Name_Ru { get; set; }
        
        public string ExternalId { get; set; }
    }
}