﻿using System.Collections.Generic;

namespace test
{
    public class ProductParsed
    {
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public Dictionary<string, string> Properties { get; set; }
        
        public IList<string> ImageUrls { get; set; }
        
        public string Ingredients { get; set; }
        
        public string Caloric { get; set; }
        
        public string  Protein { get; set; }
        
        public string  Carbohydrates { get; set; }
        
        public string  Fat { get; set; }
        
        public string Weight { get; set; }
        
        public string ShelfLife { get; set; }
        
        public string ConditionsStorage { get; set; }
        
        public string Url { get; set; }
    }
}
