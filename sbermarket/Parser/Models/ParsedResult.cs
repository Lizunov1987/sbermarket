namespace test
{
    public class ParsedResult
    {
        public ProductParsed ProductParsed { get; set; }
        
        public ValidationErrorResult[] ValidationErrors { get; set; }
    }
}