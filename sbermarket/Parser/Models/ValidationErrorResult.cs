using Newtonsoft.Json;

namespace test
{
    public class ValidationErrorResult
    {
        [JsonProperty("Название с БД")]
        public string SourcePropertyValue { get; set; }
        
        [JsonProperty("Название с сайта")]
        public string DestinationPropertyValue { get; set; }
        
        [JsonProperty("Предупреждение")]
        public string Error { get; set; }
    }
}