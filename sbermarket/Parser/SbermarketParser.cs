using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.OffScreen;
using sbermarket;

namespace test
{
    public class SbermarketParser
    {
        private readonly ProductParsedValidator _validator;
        
        private readonly ChromiumWebBrowser _browser = new ChromiumWebBrowser();

        public SbermarketParser(ProductParsedValidator validator)
        {
            _validator = validator;
        }
        
        public Task<string> GetHtmlAsync(string url)
        {
            if (string.IsNullOrEmpty(url)) throw new ArgumentNullException(nameof(url));
            
            _browser.Load(url);
            
            var tcs = new TaskCompletionSource<string>();

            EventHandler<FrameLoadEndEventArgs> handler = null;
            handler = async (sender, e) =>
            {
                if (e.Frame.IsMain)
                {
                    _browser.FrameLoadEnd -= handler;

                    var html = await _browser.GetSourceAsync();
                    
                    //var bitmap = await _browser.ScreenshotAsync(true);
                    //DisplayBitmap(bitmap);
                    
                    tcs.SetResult(html);
                }
            };
            _browser.FrameLoadEnd += handler;

            return tcs.Task;
        }
        
        public async Task<ParsedResult> ParseProductByNameAsync(string productName)
        {
            if (string.IsNullOrEmpty(productName)) throw new ArgumentNullException(nameof(productName));
            
            var productUrls = await FindProductUrlsAsync(productName);
            if (!productUrls.Any())
                throw new Exception($"Product with name {productName} is not found in the Yandex");
            
            foreach (var productUrl in productUrls)
            {
                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient.GetAsync(productUrl);
                    if (!response.IsSuccessStatusCode)
                        continue;

                    var html = await response.Content.ReadAsStringAsync();
                    var document = new HtmlAgilityPack.HtmlDocument();
                    document.LoadHtml(html);
                    
                    // Получаем описание товара
                    var productDescription = document.DocumentNode
                        .Descendants("div")
                        .Where(x => x.Attributes["itemprop"]?.Value == "description")
                        .Select(x => x.InnerText)
                        .FirstOrDefault();
    
                    var parsedName = document.DocumentNode
                        .Descendants("h1")
                          .Where(x => x.Attributes["class"]?.Value == "_2olAT")
                          .Select(x => x.InnerText)
                          .First();
                    // Получаем все характиристики товар
                    var productProperties = document.DocumentNode
                        .Descendants("div")
                        .Where(x => x.Attributes["class"]?.Value == "product-property__line")
                        .Select(x => new
                        {
                            PropertyName = x.ChildNodes[0].LastChild.InnerText,
                            PropertyValue = x.ChildNodes[1].LastChild.InnerText
                        })
                        .Distinct()
                        .ToDictionary(key => key.PropertyName, value => value.PropertyValue);
                    
                    // Получаем изображения товара
                    var imageUrls = document.DocumentNode
                        .Descendants("img")
                        .Where(x => x.Attributes["itemprop"]?.Value == "contentUrl")
                        .Select(x => x.Attributes["src"]?.Value)
                        .ToList();
    
                    var ingredients = document.DocumentNode
                        .Descendants("div")
                        .Where(x => x.Attributes["class"]?.Value == "ingredients__text")
                        .Select(x => x.InnerText)
                        .FirstOrDefault();
                    
                    var productParsed = new ProductParsed()
                    {
                        Name = parsedName,
                        Properties = productProperties,
                        Description = productDescription,
                        ImageUrls = imageUrls,
                        Ingredients = ingredients,
                        Caloric = productProperties.TryGetValue("Калорийность", out var caloric) ? caloric : null,
                        Protein = productProperties.TryGetValue("Белки", out var protein) ? protein : null,
                        Weight = productProperties.TryGetValue("Вес", out var weight) || productProperties.TryGetValue("Объем", out weight) ? weight : null,
                        ShelfLife = productProperties.TryGetValue("Срок хранения", out var shelfLife) ? shelfLife : null,
                        Carbohydrates = productProperties.TryGetValue("Углеводы", out var carbohydrates) ? carbohydrates : null,
                        Fat = productProperties.TryGetValue("Жирность", out var fat) || productProperties.TryGetValue("Жиры", out fat) ? fat : null,
                        ConditionsStorage = productProperties.TryGetValue("Условия хранения", out var conditionsStorage) ? conditionsStorage : null,
                        Url = productUrl
                    };
    
                    var validationErrors = _validator.Validate(productName, productParsed);
                    
                    return new ParsedResult()
                    {
                        ProductParsed = productParsed,
                        ValidationErrors = validationErrors
                    };
                }
            }
            
            throw new Exception("Не найден продукт на Yandex");
        }
        
        private async Task<string[]> FindProductUrlsAsync(string productName)
        {
            var pagesCount = 2;
            
            for (var i = 0; i < pagesCount; i++)
            {
                var url = new Uri($"https://yandex.ru/search/?lr=213&text={productName.Replace("&", "%26")} сбермаркет").AbsoluteUri;
                
                var html = await GetHtmlAsync(url);
                var foundUrls = Regex
                    .Matches(html, @"https?:\/\/(www\.)?sbermarket\.ru\/[a-z]+\/\b([-a-zA-Z0-9()@:%_\+.~#?&\/=]*)")
                    .Select(x => x.Value)
                    .Where(x => !x.Contains("jpg") && !x.Contains("png"))
                    .Distinct()
                    .ToList();
                
                return foundUrls.ToArray();
            }

            return null;
        }
        
        private static void DisplayBitmap(Bitmap bitmap)
        {
            // Make a file to save it to (e.g. C:\Users\jan\Desktop\CefSharp screenshot.png)
            var screenshotPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "CefSharp screenshot" + DateTime.Now.Ticks + ".png");

            Console.WriteLine();
            Console.WriteLine("Screenshot ready. Saving to {0}", screenshotPath);
            
            // Save the Bitmap to the path.
            // The image type is auto-detected via the ".png" extension.
            bitmap.Save(screenshotPath);

            // We no longer need the Bitmap.
            // Dispose it to avoid keeping the memory alive.  Especially important in 32-bit applications.
            bitmap.Dispose();

            Console.WriteLine("Screenshot saved. Launching your default image viewer...");

            // Tell Windows to launch the saved image.
            Process.Start(new ProcessStartInfo(screenshotPath)
            {
                // UseShellExecute is false by default on .NET Core.
                UseShellExecute = true
            });

            Console.WriteLine("Image viewer launched.  Press any key to exit.");
        }
    }
}