using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace test
{
    public class ProductParsedValidator
    {
        private double? GetFatInProp(string name) //в граммах
        {
            if (!string.IsNullOrEmpty(name))
            {
                if(!double.TryParse(Regex.Match(name, @"(\d{1,2}) ?(%|[Гг][Рр]?)").Groups[1].Value, out double fat))
                {
                    return null;
                }
                return Math.Round(fat, 1);
            }

            return null;
        }
        
        private double? GetFat(string name)
        {
            if(!double.TryParse(Regex.Match(name, @"(\d{1,2}) ?(%)").Groups[1].Value, out double fat))
            {
                return null;
            }
            return Math.Round(fat, 1);
        }
        
        private double? GetWeight(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                var regexResult = Regex.Match(name, @"(\d+([.,]\d+)?) ?([Кк]?[Гг][Рр]?|[Мм]?[Лл])");
                
                if (!double.TryParse(regexResult.Groups[1].Value, out double weight))
                    return null;
                
                weight = regexResult.Groups[3].Value.ToLower().OneOf("г", "гр", "мл") ? 
                    weight / 1000 : 
                    weight;

                return Math.Round(weight, 2);
            }

            return null;
        }
        
        public ValidationErrorResult[] Validate(string productName, ProductParsed productParsed)
        {
            var result = new List<ValidationErrorResult>();
            if (productName.GetLevenshteinDistance(productParsed.Name) == 0)
                return result.ToArray();

            //поиск и сравнение жирности
            var fat1 = GetFat(productName);
            var fat2 = GetFat(productParsed.Name) ?? GetFatInProp(productParsed.Fat);
            if (fat1.HasValue && fat2.HasValue && Math.Abs(fat1.Value - fat2.Value) > 0.01) 
            {
                result.Add(new ValidationErrorResult()
                {
                    SourcePropertyValue = productName,
                    DestinationPropertyValue = productParsed.Name,
                    Error = $"Несоответствие свойств товаров. Жирность отличается"
                });
            }

            //поиск и сравнение веса (объема)
            var weight1 = GetWeight(productName);
            var weight2 = GetWeight(productParsed.Name) ?? GetWeight(productParsed.Weight);
            if (weight1.HasValue && weight2.HasValue && Math.Abs(weight1.Value - weight2.Value) > 0.01) 
            {
                result.Add(new ValidationErrorResult()
                {
                    SourcePropertyValue = productName,
                    DestinationPropertyValue = productParsed.Name,
                    Error = $"Несоответствие свойств товаров. Граммовка отличается"
                });
            }
            
            var srcWords = productName.ToCyrilyc()
                .Split(new [] { ' ', '-', '_' })
                .Where(x => x.Length > 2)
                .Select(x => x.Replace("(", "").Replace(")", ""))
                .Where(x => x.All(char.IsLetter))
                .Select(x => x.ToLower())
                .Distinct()
                .ToList();
            
            var destWords = productParsed.Name.ToCyrilyc()
                .Split(new [] { ' ', '-', '_' })
                .Select(x => x.Replace("(", "").Replace(")", ""))
                .Where(x => x.Length > 2)
                .Where(x => x.All(char.IsLetter))
                .Select(x => x.ToLower())
                .Distinct()
                .ToList();

            if (srcWords.Count != destWords.Count)
            {
                result.Add(new ValidationErrorResult()
                {
                    SourcePropertyValue = productName,
                    DestinationPropertyValue = productParsed.Name,
                    Error = $"Несоответствие названий товаров. Количество слов отличается"
                }); 
                
                return result.ToArray();
            }
            
            var totalNotMatchInWords = 0;
            for(var i = 0; i < srcWords.Count; i++)
            {
                var srcWord = srcWords[i];
                var minDistance = destWords.Min(x => x.GetLevenshteinDistance(srcWord));

                totalNotMatchInWords += minDistance;
            }
            
            if(((double)totalNotMatchInWords / srcWords.Count) >= 1)
                result.Add(new ValidationErrorResult()
                {
                    SourcePropertyValue = productName,
                    DestinationPropertyValue = productParsed.Name,
                    Error = $"Несоответствие названий товаров. Количество несоответствий: {totalNotMatchInWords}"
                });

            return result.ToArray();
        }
    }
}