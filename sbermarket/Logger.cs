﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using test;

namespace sbermarket
{
    public class Logger : IDisposable
    {
        public void Write(ProductParsed productParsed)
        {
            var log = JsonConvert.SerializeObject(productParsed, Formatting.Indented);
            
            _streamWriter.Write(log);
            _streamWriter.WriteLine();
            _streamWriter.WriteLine();
        }
        
        public void Write(string log, bool toFile = false)
        {
            Console.WriteLine(log);
            
            if (toFile)
            {
                _streamWriter.Write(log);
                _streamWriter.WriteLine();
            }
        }
        

        public void Write(ValidationErrorResult[] validationErrorResults)
        {
            var log = JsonConvert.SerializeObject(validationErrorResults, Formatting.Indented);

            _streamWriter.Write(log);
            _streamWriter.WriteLine();
            _streamWriter.WriteLine();
            Console.WriteLine(log);
        }

        private readonly StreamWriter _streamWriter;
        
        public Logger(string path)
        {
            _streamWriter = File.CreateText(path);
        }
        
        public void Dispose()
        {
            _streamWriter.Dispose();
        }
    }
}
