﻿
using System.Text;

namespace test
{
    public static class StringExtensions
    {
        public static int GetLevenshteinDistance(this string s1, string s2)
        {
            return new LevenshteinDistance().Distance(s1.ToLower(), s2.ToLower());
        }
        public static string ToCyrilyc(this string s1)
        {
            string eng = "abcdefghijklmnopqrstuvwxyz";
            string rus = "абкдефгхижклмнопкрстуввкйз";
            StringBuilder sb = new StringBuilder(s1.Length);
            foreach (var item in s1.ToLower())
            {
                bool found = false;
                for (int i = 0; i < eng.Length; i++)
                {
                    if (item == eng[i])
                    {
                        sb.Append(rus[i]);
                        found = true;
                        break;
                    }

                }
                if (!found)
                    sb.Append(item);
            }
            return sb.ToString();

        }
    }
}
