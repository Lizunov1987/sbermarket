﻿using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace test
{
	public static class HttpClientExtensions
	{
		public static async Task<T> PutJsonAsync<T>(this HttpClient http, string url, object data)
		{
			var body = new StringContent(data.ToJson(), Encoding.UTF8, "application/json");

			using (var response = await http.PutAsync(url, body))
			{
				response.EnsureSuccessStatusCode();

				var jsonString = await response.Content.ReadAsStringAsync();
				return jsonString.FromJson<T>();
			}
		}
		
		public static async Task<T> PostJsonAsync<T>(this HttpClient http, string url, object data)
		{
			var body = new StringContent(data.ToJson(), Encoding.UTF8, "application/json");

			using (var response = await http.PostAsync(url, body))
			{
				response.EnsureSuccessStatusCode();

				var jsonString = await response.Content.ReadAsStringAsync();
				return jsonString.FromJson<T>();
			}
		}

		public static async Task<JObject> PostJsonAsync(this HttpClient http, string url, object data)
		{
			var body = new StringContent(data.ToJson(), Encoding.UTF8, "application/json");

			using (var response = await http.PostAsync(url, body))
			{
				response.EnsureSuccessStatusCode();

				var jsonString = await response.Content.ReadAsStringAsync();
				return jsonString.ToJsonObject();
			}
		}

		public static async Task<T> GetJsonAsync<T>(this HttpClient http, string url)
		{
			using(var response = await http.GetAsync(url))
			{
				response.EnsureSuccessStatusCode();

				var jsonString = await response.Content.ReadAsStringAsync();
				return jsonString.FromJson<T>();
			}
		}

		public static async Task<JObject> GetJsonAsync(this HttpClient http, string url)
		{
			using (var response = await http.GetAsync(url))
			{
				response.EnsureSuccessStatusCode();

				var jsonString = await response.Content.ReadAsStringAsync();
				return jsonString.ToJsonObject();
			}
		}

		public static async Task<JArray> GetJsonArrayAsync(this HttpClient http, string url)
		{
			using (var response = await http.GetAsync(url))
			{
				response.EnsureSuccessStatusCode();

				var jsonString = await response.Content.ReadAsStringAsync();
				return jsonString.ToJsonArray();
			}
		}

		public static async Task<string> GetStringSafeAsync(this HttpClient client, string url, int? encodingPage = null)
		{
			using (var response = await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead))
			{
				response.EnsureSuccessStatusCode();

				if (encodingPage == null)
					return await response.Content.ReadAsStringAsync();
				else
				{
					var enc = CodePagesEncodingProvider.Instance.GetEncoding((int)encodingPage);

					using (var stream = await response.Content.ReadAsStreamAsync())
					{
						using (var read = new StreamReader(stream, enc))
						{
							return await read.ReadToEndAsync();
						}
					}
				}
			}
		}
	}
}
