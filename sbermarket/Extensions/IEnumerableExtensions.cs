using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach(var item in source)
                action(item);

			return source;
        }

        public static async Task ForEachAsync<T>(this IEnumerable<T> source, Func<T, Task> action) =>
            await Task.WhenAll(source.Select(x => action(x)));

		public static IEnumerable<TSource> ExceptBy<TSource, TKey>(
			this IEnumerable<TSource> first,
			IEnumerable<TSource> second,
			Func<TSource, TKey> keySelector) => ExceptBy(first, second, keySelector, null);

		public static IEnumerable<TSource> ExceptBy<TSource, TKey>(
			this IEnumerable<TSource> first,
			IEnumerable<TSource> second,
			Func<TSource, TKey> keySelector,
			IEqualityComparer<TKey> keyComparer)
		{
			if (first == null) throw new ArgumentNullException(nameof(first));
			if (second == null) throw new ArgumentNullException(nameof(second));
			if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));

			return _(); IEnumerable<TSource> _()
			{
				// TODO Use ToHashSet
				var keys = new HashSet<TKey>(second.Select(keySelector), keyComparer);
				foreach (var element in first)
				{
					var key = keySelector(element);
					if (keys.Contains(key))
						continue;
					yield return element;
					keys.Add(key);
				}
			}
		}

		public static bool HasDuplicates<T>(this IEnumerable<T> source) =>
			HasDuplicates(source, EqualityComparer<T>.Default);

		public static bool HasDuplicates<T>(this IEnumerable<T> source, IEqualityComparer<T> comparer)
		{
			if (comparer == null) throw new ArgumentNullException(nameof(comparer));

			var set = new HashSet<T>(comparer);

			foreach (var s in source)
				if (!set.Add(s))
					return true;

			return false;
		}
	}
}