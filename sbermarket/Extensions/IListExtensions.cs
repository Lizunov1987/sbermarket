﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace test
{
	public static class IListExtensions
	{
		public static void RemoveRange<T>(this IList<T> @this, IEnumerable<T> removedItems) =>
			removedItems.ForEach(x => @this.Remove(x));
		
		public static void AddRange<T>(this IList<T> @this, IEnumerable<T> items)
		{
			if (@this is List<T> list)
				list.AddRange(items);
			else
				foreach (var item in @this)
					@this.Add(item);
		}

		public static void AddRange<T>(this IList<T> @this, params T[] items) =>
			@this.AddRange(items.AsEnumerable());
		
		public static int FindIndex<T>(this IList<T> @this, Predicate<T> match)
			=> @this.FindIndex(0, @this.Count, match);

		public static int FindIndex<T>(this IList<T> @this, int startIndex, Predicate<T> match)
			=> @this.FindIndex(startIndex, @this.Count - startIndex, match);

		public static int FindIndex<T>(this IList<T> @this, int startIndex, int count, Predicate<T> match)
		{
			if ((uint) startIndex > (uint) @this.Count) throw new ArgumentOutOfRangeException(nameof(startIndex));
			if (count < 0 || startIndex > @this.Count - count) throw new ArgumentOutOfRangeException(nameof(count));
			if (match == null)  throw new ArgumentNullException(nameof(match));

			var endIndex = startIndex + count;
			for (var i = startIndex; i < endIndex; i++)
			{
				if (match(@this[i])) return i;
			}
			return -1;
		}
	}
}
