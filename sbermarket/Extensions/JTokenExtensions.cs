﻿using Newtonsoft.Json.Linq;

namespace test
{
	public static class JTokenExtensions
	{
		public static bool FieldExists(this JToken source, string propertyName) =>
			source[propertyName] != null;


		public static bool TryGetValue(this JToken source, string propertyName, out JToken value)
		{
			var jObject = (JObject)source;
			return jObject.TryGetValue(propertyName, out value);
		}

		public static bool TryGetValue<T>(this JToken source, string propertyName, out T value)
		{
			var jObject = (JObject)source;
			if (!jObject.TryGetValue(propertyName, out var jToken))
			{
				value = default(T);
				return false;
			}

			try
			{
				value = jToken.ToObject<T>();
				return true;
			}
			catch
			{
				value = default(T);
				return false;
			}
		}

		public static bool IsNullOrEmpty(this JToken token)
		{
			return (token == null) ||
				   (token.Type == JTokenType.Array && !token.HasValues) ||
				   (token.Type == JTokenType.Object && !token.HasValues) ||
				   (token.Type == JTokenType.String && token.ToString() == string.Empty) ||
				   (token.Type == JTokenType.Null);
		}
	}
}
