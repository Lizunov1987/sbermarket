﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace test
{
	public static class JsonExtensions
	{
		public static string ToJson(this object source) 
			=> JsonConvert.SerializeObject(
				source, 
				new JsonSerializerSettings() 
				{
					NullValueHandling = NullValueHandling.Ignore,
					ContractResolver = new CamelCasePropertyNamesContractResolver()
				});

		public static T FromJson<T>(this string source) => 
			JsonConvert.DeserializeObject<T>(source);

		public static T FromJson<T>(this JToken source) => 
			JsonConvert.DeserializeObject<T>(source.ToString());

		public static T FromJson<T>(this string source, params JsonConverter[] converters) =>
			JsonConvert.DeserializeObject<T>(source, converters);

		public static JObject ToJsonObject(this string source) => 
			(JObject)JsonConvert.DeserializeObject(source);

		public static JArray ToJsonArray(this string source) =>
			(JArray)JsonConvert.DeserializeObject(source);
	}
}
