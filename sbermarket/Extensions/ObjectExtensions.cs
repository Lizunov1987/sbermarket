﻿using System;
using System.Linq;

namespace test
{
	public static class ObjectExtensions
	{
		public static T ChangeType<T>(this object source)
		{
			var type = typeof(T);
			if (type.IsEnum)
			{
				return source switch
				{
					string x => (T)Enum.Parse(type, x),
					int x => (T)Enum.ToObject(type, x),
					_ => throw new InvalidCastException()
				};
			}
			
			return (T)Convert.ChangeType(source, typeof(T));
		}

		public static bool OneOf<T>(this T source, params T[] variants) => variants.Contains(source);
	}
}
